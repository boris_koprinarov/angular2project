
'use strict'

module.exports = [
    {
        plugin: {
            register: require('hapi-auth-basic'),
            options: {}
        },
        callback: (err) => {
            if (err) {
                throw err;
            };
        }
    }
]