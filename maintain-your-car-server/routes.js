'use strict'

const Users = require('./handlers/users');
const Cities = require('./handlers/cities');
const UserSchema = require('./schemas/userSchemas')

module.exports = [
    {
        method: 'GET',
        path: '/api/users/{id}',
        config: {
            auth: {
                strategy: 'jwt'
            },
            handler: Users.findOne
        }
    }, {
        method: 'GET',
        path: '/api/cities',
        handler: Cities.find
    }, {
        method: 'POST',
        path: '/api/users/addOne',
        config: {
            pre: [
                { method: Users.verifyUniqueUser }
            ],

            handler: Users.addOne,

            validate: {
                payload: UserSchema.createUserSchema
            }
        }
    }, {
        method: 'POST',
        path: '/api/users/authenticate',
        config: {
            // pre: [
            //     { method: Users.verifyUniqueUser }
            // ],

            handler: Users.verifyCredentials,

            validate: {
                payload: UserSchema.createAuthSchema
            }
        }
    }

]