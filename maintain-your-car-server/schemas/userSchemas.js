'use strict';

const Joi = require('joi');

const createUserSchema = Joi.object({
  firstName: Joi.string().alphanum().min(2).max(30).required(),
  lastName: Joi.string().alphanum().min(2).max(30).required(),
  email: Joi.string().email().required(),
  pass: Joi.string().required(),
  location: Joi.number().required()
});
const createAuthSchema = Joi.object({
  email: Joi.string().email().required(),
  pass: Joi.string().required(),
});
exports.createUserSchema = createUserSchema;
exports.createAuthSchema = createAuthSchema;