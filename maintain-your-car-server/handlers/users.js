'use strict';

 const Boom = require('boom');
 const UserModel = require('../models/user-model');
 const UserRes = require('../models/userRes');
 const uuidV1 = require('uuid/v1');
 const Utils = require('./utils');

exports.find = function (request, reply) {

    let sql = 'SELECT * FROM users';
    const params = [];
    //     if (request.query.cuisine) {
    //             sql += ' WHERE cuisine = ?';
    //             params.push(request.query.cuisine);
    //     }
    this.db.all(sql, params, (err, results) => {
        if (err) {
            throw err;
        }
        reply(results);
    });
};

exports.findOne = function (request, reply) {

    this.db.get('SELECT * FROM users WHERE id = ?', [request.params.id], (err, result) => {
        if (err) {
            throw err;
        }
        if (typeof result !== 'undefined') {
             return reply( new UserRes(result.id, result.firstName, result.lastName, result.email, result.location)).code(201);
        }

        return reply('Not found').code(404);
    });
};

exports.addOne = function (request, reply) {

    let user = new UserModel();
    user.id = uuidV1();
    user.firstName = request.payload.firstName;
    user.lastName = request.payload.lastName;
    user.email = request.payload.email;
    user.location = request.payload.location;

    Utils.hashPassword(request.payload.pass, (err, hash) => {
        if (err) {
            throw Boom.badRequest(err);
        }
        user.pass = hash;

        this.db.run('INSERT INTO users (id, firstName, lastName, pass, email, location) values (?, ?, ?, ?, ?, ?)', [user.id, user.firstName, user.lastName, user.pass, user.email, user.location], (err) => {
            if (err) {
                throw Boom.badRequest(err);;
            } 
            if(err == null) {
                return reply( new UserRes(user.id, user.firstName, user.lastName,  user.email, user.location, Utils.createToken(user))).code(201);
            }
        });
    });
};

exports.verifyUniqueUser = function (request, reply) {

    this.db.get('SELECT * FROM users WHERE email = ?', [request.payload.email], (err, result) => {
        if (err) {
            throw err;
        }
        if (typeof result !== 'undefined') {
            return reply(Boom.badRequest('Email taken'));
        }
        return reply(request.payload);
    });
};

exports.verifyCredentials = function (request, reply) {

    this.db.get('SELECT * FROM users WHERE email = ?', [request.payload.email], (err, result) => {
        if (err) {
            throw err;
        }
        if (typeof result !== 'undefined') {
            Utils.comparePassword(request.payload.pass, result.pass, (err, retValue) => {
                if (err) {
                    throw Boom.badRequest(err);
                } 
                if (retValue == true) {
                    return reply( new UserRes(result.id, result.firstName, result.lastName, result.email, result.location, Utils.createToken(result))).code(201);
                } else {
                    return reply(Boom.badRequest('Incorrect password!'));
                }
            });
        } else {
            return reply(Boom.badRequest('Incorrect email!'));
        } 
    });
};



