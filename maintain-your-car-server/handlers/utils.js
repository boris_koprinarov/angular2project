
const jwt = require('jsonwebtoken');
const secret = require('../config');
const bcrypt = require('bcrypt-nodejs');

exports.hashPassword = function (password, cb) {
    bcrypt.hash(password, null, null, function(err, hash) {
         return cb(err, hash);
    });
}

exports.comparePassword = function (password, userPassword, cb) {
    bcrypt.compare(password, userPassword, function(err, result) {
         return cb(err, result);
    });
}

exports.createToken = function (user) {
  // Sign the JWT
  return jwt.sign({ id: user.id, email: user.email }, secret, { algorithm: 'HS256', expiresIn: "1h" } );
}
