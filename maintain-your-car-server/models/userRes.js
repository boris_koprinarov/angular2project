'use strict'
class UserRes{

    constructor(id, firstName, lastName, email, location, jwt){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.location = location;
        this.jwt = jwt;
    }

}
module.exports = UserRes;