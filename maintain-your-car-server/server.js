'use strict';

class Server {

    constructor() {

        const PORT = 8000;
        const HOST = '192.168.1.102';
        const Hapi = require('hapi');
        this.server = new Hapi.Server();

        this.server.connection({
            host: HOST,
            port: PORT,
            routes: { cors: true }
        });
    }

    start() {
        let plugins = require('./plugins');

        for (let key in plugins) {
            this.server.register(plugins[key].plugin, plugins[key].callback.call(this.server));
        }

        var Sqlite3 = require('sqlite3').verbose();
        const db = new Sqlite3.Database('./carmaintain.sqlite');
        this.server.bind({ db: db });

        const Boom = require('boom');


        const secret = require('./config');
        this.server.register(require('hapi-auth-jwt'), (err) => {
            this.server.auth.strategy('jwt', 'jwt', {
                key: secret,
                verifyOptions: { algorithms: ['HS256'] }
            });
        });


        this.server.start((err) => {

            this.server.route(require('./routes'));


            if (err) {
                throw err;
            }
            console.log('Server running at:', this.server.info.uri);
        });
    }
}

module.exports = Server;