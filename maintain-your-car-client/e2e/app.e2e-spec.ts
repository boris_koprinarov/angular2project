import { MaintainYourCarClientPage } from './app.po';

describe('maintain-your-car-client App', function() {
  let page: MaintainYourCarClientPage;

  beforeEach(() => {
    page = new MaintainYourCarClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
