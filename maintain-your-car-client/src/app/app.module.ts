import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { LocalStorageModule } from 'angular-2-local-storage';

import { AppComponent } from './app.component';
import { RegisterComponent } from './core/components/register/register.component';
import { UserService } from './core/services/user.service';
import { CityService } from './core/services/city.service';
import { AuthService } from './core/services/auth.service';
import { LoggedInGuardService } from './core/services/logged-in-guard.service';
import { NavigationComponent } from './core/components/navigation/navigation.component';
import { LoginComponent } from './core/components/login/login.component';
import { LoggedInComponent } from './core/components/logged-in/logged-in.component';
import { LogoutComponent } from './core/components/logout/logout.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    NavigationComponent,
    LoginComponent,
    LoggedInComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    SimpleNotificationsModule,
    LocalStorageModule.withConfig({
            prefix: 'car-maint-app',
            storageType: 'localStorage'
        }),
    RouterModule.forRoot(NavigationComponent.appRoutes)
  ],
  providers: [UserService, CityService, AuthService, LoggedInGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
