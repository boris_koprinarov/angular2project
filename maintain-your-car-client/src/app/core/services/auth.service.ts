import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';

import { UserProfile } from '../models/user-profile';

@Injectable()
export class AuthService {

  private authAnnouncedSource = new Subject<UserProfile>();
  private logoutAnnouncedSource = new Subject<boolean>();

  authAnnounced$ = this.authAnnouncedSource.asObservable();
  logoutAnnounced$ = this.logoutAnnouncedSource.asObservable();
  

  public userProfile : UserProfile
  public isLoggedIn : boolean = false;
  constructor() { 

  }

  public announceAuth(profile : UserProfile) {
      this.authAnnouncedSource.next(profile);
        this.isLoggedIn = true;
  }

  public announceLogout() {
      this.logoutAnnouncedSource.next(true);
        this.isLoggedIn = false;
  }
}
