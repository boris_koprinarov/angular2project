import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/operator/catch';
import 'rxjs/operator/map';
import 'rxjs/add/observable/throw';

import { Login } from '../models/login';
import { UserProfile } from '../models/user-profile';
import { URL_CONSTANT } from '../constants/url.constants';

@Injectable()
export class UserService {

  private userLoginUrl = URL_CONSTANT.URL_ROOT + '/api/users/authenticate';

  constructor(private http: Http) { }

  login(login: Login): Observable<UserProfile> {

   // let headers = new Headers({ 'Content-Type': 'application/json' });
   // let options = new RequestOptions({ headers: headers });

    return this.http.post(this.userLoginUrl, login, {})
      .map(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }

  private handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.message ? body.message : body.error || JSON.stringify(body);
      errMsg = `${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    return Observable.throw(errMsg);
  }

}
