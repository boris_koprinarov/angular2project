import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { CityService } from '../../services/city.service';
import { User } from '../../models/user';
import { City } from '../../models/city';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  model : User;
  userService : UserService;
  citiesService : CityService;
  cityList : City[];
  errorMessage : string;

  constructor(citiesService : CityService) {
    this.citiesService = citiesService; 
    this.model = new User('', '', '', '', '', '');
  }
  
  ngOnInit() {
    this.getCities();
  }

  private getCities() {
        this.citiesService.getCities()
            .subscribe(
            cities => {
            this.cityList = cities;
            },
            error => this.errorMessage = <any>error);
    }

  submitUser():void{
    let b = 55;
    let bbb = this.model;
    console.log(b);
  }

}
