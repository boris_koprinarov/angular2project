import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RegisterComponent } from '../register/register.component';
import { LoggedInComponent } from '../logged-in/logged-in.component';
import { LogoutComponent } from '../logout/logout.component';
import { LoginComponent } from '../login/login.component';
import { AuthService } from '../../services/auth.service';
import { UserProfile } from '../../models/user-profile';
import { LoggedInGuardService } from './../../services/logged-in-guard.service';

import { AlertModule } from 'ng2-bootstrap';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  public options = {
    position: ["bottom", "left"],
    timeOut: 5000,
    lastOnBottom: true
  }

  private userProfile : UserProfile;

  static appRoutes: Routes = [
  { path: 'registration', component: RegisterComponent, outlet: 'navigation' },
  { path: 'login', component: LoginComponent , outlet: 'navigation'},
  { path: 'logged-in', component: LoggedInComponent , outlet: 'navigation'},
  { path: 'logout', component: LogoutComponent , outlet: 'navigation'},
  // { path: 'logged-in', component: LoggedInComponent , canActivate: [LoggedInGuardService] , outlet: 'navigation'}
  // { path: 'heroes',        component: HeroListComponent },
  // { path: '',   redirectTo: '/heroes', pathMatch: 'full' },
  // { path: '**', component: PageNotFoundComponent }
];

  constructor(private authService: AuthService) { 
    authService.authAnnounced$.subscribe(
      userProfile => {
        this.userProfile = userProfile; 
      });
    authService.logoutAnnounced$.subscribe(
      logout => {
        this.userProfile = null; 
      });  
  }

  

  ngOnInit() {
  
  }

}
