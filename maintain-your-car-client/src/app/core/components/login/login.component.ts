import { Component, OnInit } from '@angular/core';
import { NotificationsService  } from 'angular2-notifications';
import { LocalStorageService } from 'angular-2-local-storage';
import { Router } from '@angular/router';

import { UserService } from '../../services/user.service';
import { AuthService } from '../../services/auth.service';

import { Login } from '../../models/login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

 model : Login;
 userService : UserService;
 notificationService : NotificationsService ;
 errorMessage : string;
 localStorageService: LocalStorageService;

  constructor(userService : UserService, notificationService: NotificationsService, localStorageService: LocalStorageService, private authService: AuthService, private router: Router,) { 
    this.model = new Login('','');
    this.userService = userService;
    this.notificationService = notificationService;
    this.localStorageService = localStorageService;
  }
  
  ngOnInit() {
  
  }

   private login() {
        this.userService.login(this.model)
            .subscribe(
            userRes => {
              this.authService.announceAuth(userRes);
              this.router.navigate([{ outlets: { navigation: 'logged-in' }}]);
            },
            error => { 
              this.notificationService.error('Error login in.', error); 
              this.errorMessage = <any>error} 
              );
    }

  submitLogin():void{
    this.login();
  }
}
