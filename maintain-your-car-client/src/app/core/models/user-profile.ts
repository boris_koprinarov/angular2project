export class UserProfile {

    public id: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public location: string;
    public jwt;

    constructor(id: string, firstName: string, lastName: string, pass: string, email: string, location: string, jwt : string) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.location = location;
        this.jwt = jwt;
    }
}