export class City {

    public id: number;
    public city: string;
    
    constructor(id: number, city: string) {
        this.id = id;
        this.city = city;
    }

}