export class Login {

    public pass: string;
    public email: string;
    
    constructor(email: string, pass: string) {
        this.pass = pass;
        this.email = email;
    }

}