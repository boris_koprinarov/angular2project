export class User {

    public id: string;
    public firstName: string;
    public lastName: string;
    public pass: string;
    public email: string;
    public location: string;

    constructor(id: string, firstName: string, lastName: string, pass: string, email: string, location: string) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pass = pass;
        this.email = email;
        this.location = location;
    }
}